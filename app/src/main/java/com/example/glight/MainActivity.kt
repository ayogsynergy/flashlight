package com.example.glight

import android.content.Context
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    val camMan by lazy { getSystemService(Context.CAMERA_SERVICE) as CameraManager }
    val turnOffImg by lazy { findViewById<ImageView>(R.id.turnOffTorchView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        turnOffImg.setOnClickListener {
            camMan.setTorchMode(camMan.cameraIdList[0], false)
        }
    }

    override fun onResume() {
        super.onResume()
        camMan.setTorchMode(camMan.cameraIdList[0], true)
    }

    override fun onPause() {
        super.onPause()
        camMan.setTorchMode(camMan.cameraIdList[0], false)
    }
}